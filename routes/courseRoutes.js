const express = require('express');
const router = express.Router();

//import course controllers
const courseControllers = require('../controllers/courseControllers');

//import auth.js
const auth = require('../auth');
	//to destructure functions in auth.js
const {verify, verifyAdmin} = auth;

//routes

//add course route
router.post('/', verify, verifyAdmin, courseControllers.addCourse);

//retrieve all course route
router.get('/', courseControllers.getAllCourses);

//update course route
router.put('/:id', verify, verifyAdmin, courseControllers.updateCourse);

//get details of a single course
router.get('/getSingleCourse/:id', courseControllers.getSingleCourse);

//change course isActive to false
router.put('/archive/:id', verify, verifyAdmin, courseControllers.updateCourseStatusToFalse);

//change course isActive to true
router.put('/activate/:id', verify, verifyAdmin, courseControllers.updateCourseStatusToTrue)

//get all active courses
router.get('/getActiveCourses', courseControllers.getActiveCourses);

//get all inactive courses
router.get('/getInactiveCourses', verify, verifyAdmin, courseControllers.getInactiveCourses)

//find courses by name
router.post('/findCoursesByName', courseControllers.findCoursesByName);

//find courses by price
router.post('/findCoursesByPrice', courseControllers.findCoursesByPrice);

//get enrolless's list
router.get('/getEnrollees/:id', verify, verifyAdmin, courseControllers.getEnrollees);


//export courseRoutes
module.exports = router;