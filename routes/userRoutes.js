const express = require('express');
const router = express.Router();

//to import user controllers
const userControllers = require("../controllers/userControllers");

//import auth.js
const auth = require('../auth');
const {verify, verifyAdmin} = auth;

//routes

//user registration route
router.post('/', userControllers.registerUser);

//get all users route
router.get('/', userControllers.getAllUsers)

//login route
router.post('/login', userControllers.loginUser);

//retrieve user details
router.get('/getUserDetails', verify, userControllers.getUserDetails);

//update user
router.put('/updateProfile', verify, userControllers.updateUser);

//check email
router.post('/checkEmailExists', userControllers.checkEmailExists);

//change admin status//log in as admin to change non-admin user
router.put('/updateAdmin/:id', verify, verifyAdmin, userControllers.updateAdmin);

//enroll registered user
router.post('/enroll', verify, userControllers.enroll);

//get enrollments of user
router.get('/getEnrollments', verify, userControllers.getEnrollments);

//export
module.exports = router;