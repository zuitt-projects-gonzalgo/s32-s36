//import modules
const bcrypt = require('bcrypt');//for password security

//import User model
const User = require('../models/User');

//import auth module
const auth = require('../auth');

//import Course model
const Course = require('../models/Course');

//controllers

//user registration

module.exports.registerUser = (req, res) => {
	console.log(req.body);//for checking

	//to hash password
		//bcrypt - adding a layer of security to your user's password
			//what bcrypt does is hash our passsword into a randomized character version of the original string
			//syntax: bcrypt.hashSync(<stringTobeHash>, <saltRounds>)
			//salt-rounds are the number of times the characters in the hash are randomized
	const hashedPW = bcrypt.hashSync(req.body.password, 10);

	//create a new user document out of our user model
	let newUser = new User({
		firstName : req.body.firstName,
		lastName : req.body.lastName,
		email : req.body.email,
		mobileNo : req.body.mobileNo,
		password : hashedPW
	});

	newUser.save()
	.then(user => res.send(user))
	.catch(err => res.send(err))
};

//retrieve all users

module.exports.getAllUsers = (req, res) => {
	User.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err))
};

//login

module.exports.loginUser = (req, res) => {
	console.log(req.body);
	//1st - find the user email
	//2nd - if the user email is found, check password
	//3rd - if we don't find the user email, send a message
	//4th - if upon checking the found user's password is the same as our input password, we will generate a "key" to access our app. If not, we will turn him away by sending a message to the client

	User.findOne({email : req.body.email})
	.then(foundUser => {
		if(foundUser === null) {
			return res.send("User does not exits");
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);
			if(isPasswordCorrect) {
				return res.send({accessToken : auth.createAccessToken(foundUser)})
			} else {
				return res.send("Password is incorrect")
			}
		}
	})
	.catch(err => res.send(err));
};

//update user
module.exports.updateUser = (req, res) => {
	let updateUser = {
		firstName : req.body.firstName,
		lastName : req.body.lastName,
		mobileNo : req.body.mobileNo,
	}

	User.findByIdAndUpdate(req.user.id, updateUser, {new : true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err))
};

//get user details
module.exports.getUserDetails = (req, res) => {
	console.log(req.user)
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))
};

//chech if email exists
module.exports.checkEmailExists = (req, res) => {
	User.findOne({email : req.body.email})
	.then(foundUser => {
		if(foundUser === null) {
			return res.send('Email is Available')
		} else {
			return res.send('Email is already registered!')
		}
	})
	.catch(err => res.send(err))
};

//change admin status(login as admin to change non-admin user) - params(userYouWantToChange)
module.exports.updateAdmin = (req, res) => {
	let updates = {
		isAdmin : true
	}

	User.findByIdAndUpdate(req.params.id, updates, {new : true})
	.then(result => res.send(result))
	.catch(err => res.send(err))
};

//enroll a user
module.exports.enroll = async (req, res) => {
	/*
		enrollment process:
		1. Look for the user by its id
			>>push the details of the course we're trying to enroll in
				>>we'll push to a new enrollment subdocument in our user
		2. Look for the course by its ID
			>>push the details of the enrolle/user who's trying to enroll
				>>we'll push to a new enrollees subdocument in our course
		3. When both saving documents are successful, we send a message to the client
	*/
	console.log(req.user.id);
	console.log(req.body.courseId);

	//checking if user is an admin, if he is an admin then he should not be able to enroll
	if(req.user.isAdmin) {
		return res.send("Action Forbidden");
	}

	/*
		Find the user:
			async - a keyword that allows us to make our function asynchronous. Which means, that instead of JS regular behaviour of running each code line by line, it will allow us to wait for the result of the function.

			await - a keyword that allows us to wait for the function to finish before proceeding
	*/

	let isUserUpdated = await User.findById(req.user.id)
	.then(user => {
		console.log(user);
		//add courseID in an object and push that object into user's enrollment array
		let newEnrollment = {
			courseId : req.body.courseId
		}

		user.enrollments.push(newEnrollment);
		return user.save().then(user => true).catch(err => err.message);
	})

	//if usUserUpdated does not contain the boolean value of true, we will stop our process and return res.send() to our client with our message
	if(isUserUpdated !== true) {
		return res.send({message : isUserUpdated})
	}

	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
		console.log(course);

		//create an object which will contain the user id of the enrollee of a course
		let enrollee = {
			userId : req.user.id
		}

		course.enrollees.push(enrollee);
		return course.save().then(course => true).catch(err => err.message);
	})

	if(isCourseUpdated !== true) {
		return res.send({message : isCourseUpdated})
	}

	if(isUserUpdated && isCourseUpdated) {
		return res.send({message : 'Enrolled Succesfully!'})
	}
};

//get enrollments of user
module.exports.getEnrollments = (req, res) => {
	console.log(req.user.id);
	User.findById(req.user.id)
	.then(result => {
		if(result.enrollments.length === 0) {
			return res.send('Not Enrolled to any Subjects')
		} else {
			res.send(result.enrollments)
		}
	})
	.catch(err => res.send(err))
};