//import course model
const Course = require('../models/Course');

//controllers

//add courses
module.exports.addCourse = (req, res) => {
	let newCourse = new Course({
		name : req.body.name,
		description : req.body.description,
		price : req.body.price
	})

	newCourse.save()
	.then(newcourse => res.send(newcourse))
	.catch(err => res.send(err))
}

//get all course

module.exports.getAllCourses = (req, res) => {
	Course.find({})
	.then(courses => res.send(courses))
	.catch(err => res.send(err))
};

//update course

module.exports.updateCourse = (req, res) => {
	let courseUpdates = {
		name : req.body.name,
		description : req.body.description,
		price : req.body.price
	}

	Course.findByIdAndUpdate(req.params.id, courseUpdates, {new : true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err))
}

//get details of single course
module.exports.getSingleCourse = (req, res) => {
	Course.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))
};

//change course isActive to false
module.exports.updateCourseStatusToFalse = (req, res) => {
	let changeToFalse = {
		isActive : false
	}

	Course.findByIdAndUpdate(req.params.id, changeToFalse, {new : true})
	.then(deactivated => res.send(deactivated))
	.catch(err => res.send(err))
};

//change course isActive to true
module.exports.updateCourseStatusToTrue = (req, res) => {
	let changeToTrue = {
		isActive : true
	}

	Course.findByIdAndUpdate(req.params.id, changeToTrue, {new : true})
	.then(activated => res.send(activated))
	.catch(err => res.send(err))
};

//get all active courses
module.exports.getActiveCourses =(req, res) => {
		Course.find({isActive : true})
		.then(activeCourses => res.send(activeCourses))
		.catch(err => res.send(err))
};

//get all inactive courses
module.exports.getInactiveCourses =(req, res) => {
		Course.find({isActive : false})
		.then(inactiveCourses => res.send(inactiveCourses))
		.catch(err => res.send(err))
};

//find courses by name
module.exports.findCoursesByName = (req, res) => {
	Course.find({name : {$regex : req.body.name, $options : '$i'}})
	.then(result => {
		if(result.length === 0) {
			return res.send('No Courses found')
		} else {
			return res.send(result)
		}
	})
	.catch(err => res.send(err))
};

//find courses by price
module.exports.findCoursesByPrice = (req, res) => {
	Course.find({price : req.body.price})
	.then(result => {
		if(result.length === 0) {
			return res.send('No course found')
		} else {
			return res.send(result)
		}
	})
	.catch(err => res.send(err))
};

//get enrollees list's
module.exports.getEnrollees = (req, res) => {
	Course.findById(req.params.id)
	.then(result => {
		if(result.enrollees.length === 0) {
			return res.send('No students Enrolled')
		} else {
			res.send(result.enrollees)
		}
	})
	.catch(err => res.send(err))
};