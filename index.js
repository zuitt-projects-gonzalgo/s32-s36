//packages to be installed and load modules(require)
const express = require('express');
const mongoose = require('mongoose');
	//allow backend to connect in frontend
const cors = require('cors');//cors origin resource sharing

//make a port and server
const port = 4000;
const app = express();

//to import routes
const userRoutes = require('./routes/userRoutes');
const courseRoutes = require('./routes/courseRoutes');

//connect to database
mongoose.connect("mongodb+srv://admin_gonzalgo:admin169@gonzalgo-169.griu6.mongodb.net/bookingAPI169?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});
//to create a notification to the db if succesful or not
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection Error'));
db.once('open', () => console.log('Connected to MongoDB'));

//middleware
app.use(express.json());
app.use(cors());
	//group routes
app.use('/users', userRoutes);
app.use('/courses', courseRoutes);


//to load server and listen to port
app.listen(port, () => console.log(`Server is running on localhost:${port}`))