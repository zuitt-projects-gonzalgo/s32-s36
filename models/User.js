const mongoose = require('mongoose');

let userSchema = new mongoose.Schema({

	firstName : {
		type : String,
		required : [true, "What do I call you?"]
	},
	lastName : {
		type : String,
		required : [true, "Which family you belong?"]
	},
	email : {
		type : String,
		required : [true, "Email is Required"]
	},
	password : {
		type : String,
		required : [true, "Password is Required"]
	},
	mobileNo : {
		type : String,
		required : [true, "Mobile No. is Required"]
	},
	isAdmin :	{
		type : Boolean,
		default : false
	},
	enrollments : [
		{
			courseId : {
				type : String,
				required : [true, "Course ID is required"]
			},
			status : {
				type : String,
				default : "Enrolled"
			},
			dateEnrolled : {
				type : Date,
				default : new Date()
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema);